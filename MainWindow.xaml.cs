﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace Lab7.WpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string output = "";
        string operation = "";
        decimal temp = 0.0m;
        decimal temp2 = 0.0m;
        //bool isTemp2 = false;
        string prevOperation = "";
        decimal temp3 = 0.0m;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Num_Click(object sender, RoutedEventArgs e)
        {
            Blokuj(true); // uaktywnienie zablokowanych przycisków
            // wyczyszczenie zmiennych w przypadku nieprawidłowego poprzedniego działania
            if (PoleTekstowe.Text == "Nieprawidłowe dane wejściowe‬")
                Pzeruj_Click(sender, e);
            // wyczyszczenie zmiennych w przypadku wykonania operacji "="
            if (operation=="=")
            {
                operation = "";
                temp = 0.0m;
                temp2 = 0.0m;
                output = "";
                prevOperation = "";
            }
            // zrzutowanie sendera na przycisk
            Button button = (Button)sender;
            // uaktualnienie drugiego pola tekstowego w przypadku operacji różnej niż "Exp"
            if (operation != "Exp" && (string)button.Content != ".")
            {
                if(PoleTekstowe2.Text!="0")
                    PoleTekstowe2.Text += button.Content;
            }
            // sprawdzenie wstawiania kropki do liczby
            if ((string)button.Content == ".")
            {
                if (!output.Contains('.'))
                {
                    if (output.Length == 0)
                    {
                        output += "0";
                        PoleTekstowe2.Text += "0";
                    }
                    output += ".";
                    PoleTekstowe2.Text += ".";
                }
            }
            else // dopisanie cyfry do naszej liczby
            {
                if ((string)button.Content != "0")
                    output += button.Content;
                else
                {
                    if (output.Length <= 1)
                    {
                        output = "0";
                        PoleTekstowe2.Text = "0";
                    }
                    else
                    {
                        output += button.Content;
                        //PoleTekstowe2.Text += button.Content;
                    }
                }
            }
            if (operation != "Exp") // uaktualnienie wpisanej liczby w przypadku operacji różnej niż "Exp"
                PoleTekstowe.Text = output;
            else // uaktualnienie wpisanej liczby w przypadku operacji "Exp"
            {
                int index = PoleTekstowe.Text.IndexOf('+');
                PoleTekstowe.Text = PoleTekstowe.Text.Substring(0, index + 1) + output;
            }
        }

        private void Poperation_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if ((string)button.Content=="=") // wykonanie działania "="
            {
                // zerowanie w przypadku nie prawidłowej operacji
                if(PoleTekstowe.Text=="Nie można dzielić przez zero" || PoleTekstowe.Text == "Nieprawidłowe dane wejściowe‬")
                    Pzeruj_Click(sender,e);
                // wykoanie działania dla operacji "Exp"
                if(operation=="Exp")
                {
                    temp2 = decimal.Parse(output, CultureInfo.InvariantCulture);
                    Pwynik_Click();
                }
                else // wykoanie wszystkich innych operacji
                {
                    if (prevOperation == "") // ustawnie poprzedniej operacji
                    {
                        PoleTekstowe2.Text += button.Content;
                        temp = decimal.Parse(PoleTekstowe.Text!=""? PoleTekstowe.Text: "0", CultureInfo.InvariantCulture);
                        Pwynik_Click();
                        prevOperation = operation;
                        operation = "=";
                    }
                    else
                    {
                        // wykonanie działania uzyskania wyniku dla operacji różnych niż "jednoArgs"
                        if(prevOperation!="jednoArgs" && prevOperation!="=")
                        {
                            if(prevOperation!="")
                            {
                                temp = decimal.Parse(output != "" ? output : "0", CultureInfo.InvariantCulture);
                                Pwynik_Click();
                                temp = temp2;
                                temp2 = temp3;
                                //prevOperation = "";
                            }
                            operation = prevOperation;
                            Pwynik_Click();
                            operation = "=";
                            Pwynik_Click();
                        }
                    }
                    output = "";
                    PoleTekstowe2.Text = "";
                }
            }
            else // wykonanie wszystkich innych operacji, które są dwuargumentowe
            {
                if (output != "") // sczytanie pierwszej wartości dla działania w przypadku wystepowania cyfr w wejściu
                {
                    temp = decimal.Parse(output, CultureInfo.InvariantCulture);
                    if (operation == "") // sprawdzenie czy był już wciśnięty wczesniej operator matematyczny
                    {
                        output = "";
                        operation = (string)button.Content;
                        temp2 = temp;
                        temp = 0.0m;
                        // uaktualnianie górnego pola tekstowego zawierającego wprowadzane działanie
                        if (operation == "x^y")
                        {
                            PoleTekstowe2.Text = temp2 + "^";
                        }
                        else
                            PoleTekstowe2.Text = temp2 + (string)button.Content;
                        if (prevOperation != "") prevOperation = ""; // skasowanie wystepowania wcześniejszej operacji
                    }
                    else // wykonanie działania zależnego od wciśniętego przycisku działania
                    {
                        if (operation == "")
                        {
                            operation = (string)button.Content;
                            Pwynik_Click();
                            if (prevOperation != "") prevOperation = "";
                            output = "";
                        }
                        else
                        {
                            //zapamietanie stanu działań
                            if (((string)button.Content == "*" || (string)button.Content == "/") && (operation == "+" || operation == "-"))
                            {
                                prevOperation = operation;
                                operation = (string)button.Content;
                                if (operation == "x^y")
                                {
                                    PoleTekstowe2.Text += "^";
                                }
                                else
                                    PoleTekstowe2.Text += (string)button.Content;
                                temp3 = temp2;
                                temp2 = temp;
                            }
                            else
                            {
                                if (prevOperation != "")
                                {
                                    Pwynik_Click();
                                    temp = temp2;
                                    temp2 = temp3;
                                    operation = prevOperation;
                                    if (((string)button.Content == "*" || (string)button.Content == "/") && (operation == "+" || operation == "-"))
                                    {
                                        prevOperation = operation;
                                        operation = (string)button.Content;
                                        if (operation == "x^y")
                                        {
                                            PoleTekstowe2.Text += "^";
                                        }
                                        else
                                            PoleTekstowe2.Text += (string)button.Content;
                                        temp3 = temp2;
                                        temp2 = temp;
                                    }
                                    else
                                    {
                                        Pwynik_Click();
                                        operation = (string)button.Content;
                                        if (operation == "x^y")
                                        {
                                            PoleTekstowe2.Text += "^";
                                        }
                                        else
                                            PoleTekstowe2.Text += (string)button.Content;
                                        if (prevOperation != "") prevOperation = "";
                                    }
                                }
                                else
                                {
                                    Pwynik_Click();
                                    operation = (string)button.Content;
                                    if (operation == "x^y")
                                    {
                                        PoleTekstowe2.Text += "^";
                                    }
                                    else
                                        PoleTekstowe2.Text += (string)button.Content;
                                }
                            }
                            output = "";
                        }
                    }
                }
                else // sczytanie pierwszej liczby do działania matematycznego
                {
                    operation = (string)button.Content;
                    temp2 = decimal.Parse((PoleTekstowe.Text ==""? "0": PoleTekstowe.Text), CultureInfo.InvariantCulture);
                    if(operation=="x^y")
                    {
                        PoleTekstowe2.Text = temp2 + "^";
                    }
                    else
                        PoleTekstowe2.Text = temp2 + (string)button.Content;
                    if (prevOperation != "") prevOperation = "";
                    output = "";
                }
            }
        }
            
        private void Pzeruj_Click(object sender, RoutedEventArgs e)
        {
            Blokuj(true);
            output = "";
            temp = 0.0m;
            temp2 = 0.0m;
            prevOperation = "";
            operation = "";
            PoleTekstowe.Text = "0";
            PoleTekstowe2.Text = "";
        }
        private void Pwynik_Click()
        {
            switch(operation)
            {
                case "+":
                    temp2 = temp + temp2;
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "-":                   
                    temp2 = temp2 - temp;
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "*":
                    temp2 = temp * temp2;
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "/":
                    if(output != "0")
                    {
                        temp2 = temp2 / temp;
                        output = temp2.ToString(CultureInfo.InvariantCulture);
                        PoleTekstowe.Text = output;
                    }
                    else
                    {
                        output = "Nie można dzielić przez zero";
                        PoleTekstowe.Text = output;
                        temp = 0.0m;
                        Blokuj(false);
                    }
                    break;
                case "n!":
                    if (temp2 >= 0)
                    {
                        temp = temp2;
                        for (int i = (int)temp - 1; i >= 1; i--)
                        {
                            temp2 *= i;
                        }
                        output = temp2.ToString(CultureInfo.InvariantCulture);
                        PoleTekstowe.Text = output;
                    }
                    else
                    {
                        output = "Nieprawidłowe dane wejściowe‬";
                        PoleTekstowe.Text = output;
                        temp = 0.0m;
                        Blokuj(false);
                    }
                    break;
                case "x^y":
                    temp2 = (decimal)Math.Pow((double)temp2, (double)temp);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "x^2":
                    temp2 = (decimal)Math.Pow((double)temp2, 2.0);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "x^3":
                    temp2 = (decimal)Math.Pow((double)temp2, 3.0);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "sqrt":
                    if(temp2>=0)
                    {
                        temp2 = (decimal)Math.Sqrt((double)temp2);
                        output = temp2.ToString(CultureInfo.InvariantCulture);
                        PoleTekstowe.Text = output;
                    }
                    else
                    {
                        output = "Nieprawidłowe dane wejściowe‬";
                        PoleTekstowe.Text = output;
                        temp = 0.0m;
                        Blokuj(false);
                    }
                    break;
                case "ysqrt(x)":
                    if (temp2 >= 0)
                    {
                        temp2 = (decimal)Math.Pow((double)temp2, 1/ (double)temp);
                        output = temp2.ToString(CultureInfo.InvariantCulture);
                        PoleTekstowe.Text = output;
                    }
                    else
                    {
                        if (temp % 2 == 1)
                        {
                            temp2 = -(decimal)Math.Pow(-(double)temp2, 1 / (double)temp);
                            output = temp2.ToString(CultureInfo.InvariantCulture);
                            PoleTekstowe.Text = output;
                        }
                        else
                        {
                            output = "Nieprawidłowe dane wejściowe‬";
                            PoleTekstowe.Text = output;
                            temp = 0.0m;
                            Blokuj(false);
                        }
                    }
                    break;
                case "10^x":
                    temp2 = (decimal)Math.Pow(10.0, (double)temp2);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "e^x":
                    temp2 = (decimal)Math.Pow(Math.E, (double)temp2);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "1/x":
                    if (output != "0" && output !="")
                    {
                        temp2 = 1.0m / temp2;
                        output = temp2.ToString(CultureInfo.InvariantCulture);
                        PoleTekstowe.Text = output;
                    }
                    else
                    {
                        output = "Nie można dzielić przez zero";
                        PoleTekstowe.Text = output;
                        temp = 0.0m;
                        Blokuj(false);
                    }
                    break;
                case "PI":
                    temp2 = (decimal)Math.PI;
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "log":
                    temp2 = (decimal)Math.Log10((double)temp2);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "ln":
                    temp2 = (decimal)Math.Log((double)temp2);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "sin":
                    temp2 = (decimal)Math.PI * temp2 / 180; // zamiana radianów na stopnie
                    temp2 = (decimal)Math.Sin((double)temp2);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "sin^(-1)":
                    if(temp2>=-1&&temp2<=1)
                    {
                        temp2 = (decimal)Math.Asin((double)temp2);
                        temp2 = 180.0m / (decimal)Math.PI * temp2; // zamiana radianów na stopnie
                        output = Math.Round(temp2, 12).ToString(CultureInfo.InvariantCulture);
                        PoleTekstowe.Text = output;
                    }
                    else
                    {
                        output = "Nieprawidłowe dane wejściowe‬";
                        PoleTekstowe.Text = output;
                        temp = 0.0m;
                        Blokuj(false);
                    }
                    break;
                case "cos":
                    temp2 = (decimal)Math.PI * temp2 / 180; // zamiana radianów na stopnie
                    temp2 = (decimal)Math.Cos((double)temp2);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "cos^(-1)":
                    if (temp2 >= -1 && temp2 <= 1)
                    {
                        temp2 = (decimal)Math.Acos((double)temp2);
                        temp2 = 180.0m / (decimal)Math.PI * temp2; // zamiana radianów na stopnie
                        output = Math.Round(temp2, 12).ToString(CultureInfo.InvariantCulture);
                        PoleTekstowe.Text = output;
                    }
                    else
                    {
                        output = "Nieprawidłowe dane wejściowe‬";
                        PoleTekstowe.Text = output;
                        temp = 0.0m;
                        Blokuj(false);
                    }
                    break;
                case "tan":
                    temp2 = (decimal)Math.PI * temp2 / 180; // zamiana radianów na stopnie
                    temp2 = (decimal)Math.Tan((double)temp2);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "tan^(-1)":
                    temp2 = (decimal)Math.Atan((double)temp2);
                    temp2 = 180.0m / (decimal)Math.PI * temp2; // zamiana radianów na stopnie
                    output = Math.Round(temp2, 12).ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "Exp":
                    temp2 = temp * (decimal)Math.Pow(10, (double)temp2);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "Mod":
                    temp2 = temp2 % temp;
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "=":
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "deg":
                    int stopnie = (int)temp2;
                    decimal reszta = temp2 - stopnie;
                    int minuty = (int)(reszta * 100);
                    decimal sekundy = (reszta * 100 - minuty) * 100;
                    temp2 = stopnie + (minuty / 60.0m + sekundy / 3600.0m);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
                case "dms":
                    stopnie = (int)temp2;
                    reszta = temp2 - stopnie;
                    minuty = (int)(reszta * 60);
                    sekundy = (reszta * 60 - minuty) * 60;
                    temp2 = stopnie + (minuty / 100.0m + sekundy / 10000.0m);
                    output = temp2.ToString(CultureInfo.InvariantCulture);
                    PoleTekstowe.Text = output;
                    break;
            }
        }
        private void Pfunkcja_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            decimal localTemp = 0.0m;
            if((string)button.Content!="PI")
            {
                localTemp = temp2;
                temp2 = decimal.Parse(PoleTekstowe.Text ==""? "0" : PoleTekstowe.Text, CultureInfo.InvariantCulture);
            }
            if(operation=="")
            {
                prevOperation = "jednoArgs";
            }
            else
            {
                prevOperation = operation;
                temp = temp2;
            }
            operation = (string)button.Content;
            switch(operation)
            {
                case "n!":
                    PoleTekstowe2.Text = "fact(" + PoleTekstowe.Text + ")";
                    break;
                case "sqrt":
                    PoleTekstowe2.Text = "sqrt(" + PoleTekstowe.Text + ")";
                    break;
                case "x^2":
                    PoleTekstowe2.Text = "sqr(" + PoleTekstowe.Text + ")";
                    break;
                case "x^3":
                    PoleTekstowe2.Text = "cube(" + PoleTekstowe.Text + ")";
                    break;
                case "10^x":
                    PoleTekstowe2.Text = "10^(" + PoleTekstowe.Text + ")";
                    break;
                case "e^x":
                    PoleTekstowe2.Text = "e^(" + PoleTekstowe.Text + ")";
                    break;
                case "1/x":
                    PoleTekstowe2.Text = "1/(" + PoleTekstowe.Text + ")";
                    break;
                case "log":
                    PoleTekstowe2.Text = "log(" + PoleTekstowe.Text + ")";
                    break;
                case "ln":
                    PoleTekstowe2.Text = "ln(" + PoleTekstowe.Text + ")";
                    break;
                case "sin":
                    PoleTekstowe2.Text = "sin(" + PoleTekstowe.Text + ")";
                    break;
                case "cos":
                    PoleTekstowe2.Text = "cos(" + PoleTekstowe.Text + ")";
                    break;
                case "tan":
                    PoleTekstowe2.Text = "tan(" + PoleTekstowe.Text + ")";
                    break;
                case "deg":
                    PoleTekstowe2.Text = "degrees(" + PoleTekstowe.Text + ")";
                    break;
                case "dms":
                    PoleTekstowe2.Text = "dms(" + PoleTekstowe.Text + ")";
                    break;
            }
            Pwynik_Click();
            if (prevOperation != "jednoArgs")
            {
                decimal t = temp;
                temp = temp2;
                temp2 = t;
            }
            if(operation!="PI")
            {
                temp2 = localTemp;
            }

            operation = "";
            output = temp2.ToString(CultureInfo.InvariantCulture);
        }

        private void Blokuj(bool zablokuj)
        {
            Pplus.IsEnabled = zablokuj;
            Pminus.IsEnabled = zablokuj;
            Prazy.IsEnabled = zablokuj;
            Pdziel.IsEnabled = zablokuj;
            Pkropka.IsEnabled = zablokuj;
            nawiasLewy.IsEnabled = zablokuj;
            nawiasPrawy.IsEnabled = zablokuj;
            Psilnia.IsEnabled = zablokuj;
            pi.IsEnabled = zablokuj;
            sqrt.IsEnabled = zablokuj;
            potega2.IsEnabled = zablokuj;
            potegaY.IsEnabled = zablokuj;
            dziesiecDoPotegi.IsEnabled = zablokuj;
            Log.IsEnabled = zablokuj;
            Exp.IsEnabled = zablokuj;
            Mod.IsEnabled = zablokuj;
            Tan.IsEnabled = zablokuj;
            Cos.IsEnabled = zablokuj;
            Sin.IsEnabled = zablokuj;
            Zmien.IsEnabled = zablokuj;
            PlusMinus.IsEnabled = zablokuj;
            jedenPrzezX.IsEnabled = zablokuj;
            eDoX.IsEnabled = zablokuj;
            ln.IsEnabled = zablokuj;
            dms.IsEnabled = zablokuj;
            deg.IsEnabled = zablokuj;
            xDo3.IsEnabled = zablokuj;
            PxSy.IsEnabled = zablokuj;
            SinDoMinusJeden.IsEnabled = zablokuj;
            CosDoMinusJeden.IsEnabled = zablokuj;
            TanDoMinusJeden.IsEnabled = zablokuj;
        }

        private void Pznak_Click(object sender, RoutedEventArgs e)
        {
            if (operation != "Exp") // zmiana znaku liczby w przypadku innej operacji niż "Exp"
            {
                temp = decimal.Parse(PoleTekstowe.Text, CultureInfo.InvariantCulture);
                temp *= -1;
                PoleTekstowe.Text = temp.ToString(CultureInfo.InvariantCulture);
                output = temp.ToString(CultureInfo.InvariantCulture);
                if (temp < 0)
                    PoleTekstowe2.Text = PoleTekstowe2.Text.Substring(0, PoleTekstowe2.Text.Length - 1) + output;
                else
                    PoleTekstowe2.Text = PoleTekstowe2.Text.Substring(0, PoleTekstowe2.Text.Length - 2) + output;
            }
            else // zmiana znaku drugiej liczby w działaniu "Exp"
            {
                if(PoleTekstowe.Text.Contains('+'))
                {
                    char[] tt = PoleTekstowe.Text.ToCharArray();
                    int index = PoleTekstowe.Text.IndexOf('+');
                    tt[index] = '-';
                    PoleTekstowe.Text = new string(tt);

                    output = "-" + output;
                }
                else
                {
                    char[] tt = PoleTekstowe.Text.ToCharArray();
                    int index = PoleTekstowe.Text.IndexOf('-');
                    tt[index] = '+';
                    PoleTekstowe.Text = new string(tt);
                    output = output.Substring(1, output.Length-1);
                }
            }
        }

        //Klawisz CE
        private void Poutput_Click(object sender, RoutedEventArgs e)
        {
            PoleTekstowe2.Text = PoleTekstowe2.Text.Substring(0, PoleTekstowe2.Text.Length - output.Length);
            output = "";
            PoleTekstowe.Text = "0";
        }

        //Klawisz BS
        private void Pusun_Click(object sender, RoutedEventArgs e)
        {
            if (output.Length > 0)
            {
                output = output.Substring(0, output.Length - 1);
                PoleTekstowe2.Text = PoleTekstowe2.Text.Substring(0, PoleTekstowe2.Text.Length - 1);
                PoleTekstowe.Text = output;
            }
            if (output == "")
                PoleTekstowe.Text = "0";
        }

        //Działanie EXP
        private void PExp_Click(object sender, RoutedEventArgs e)
        {
            if (output == "") // ustawienie pierwszej wartości do działania w przypadku zera
            {
                temp = 0.0m;
                PoleTekstowe.Text = "0.e";
            }
            else // ustawienie pierwszej wartości do działania w przypadku innej wartości niż 0
            {
                temp = decimal.Parse(PoleTekstowe.Text, CultureInfo.InvariantCulture);
                PoleTekstowe.Text = (temp > -1 && temp < 1  ? temp.ToString(CultureInfo.InvariantCulture) + "e+0" : temp.ToString(CultureInfo.InvariantCulture) + ".e+0");
            }
            operation = "Exp";
            PoleTekstowe2.Text = "";
            output = "";
        }

        private void Pzmien_Click(object sender, RoutedEventArgs e)
        {
            if(sqrt.Visibility==Visibility.Visible)
            {
                sqrt.Visibility = Visibility.Hidden;
                potega2.Visibility = Visibility.Hidden;
                potegaY.Visibility = Visibility.Hidden;
                dziesiecDoPotegi.Visibility = Visibility.Hidden;
                Sin.Visibility = Visibility.Hidden;
                Cos.Visibility = Visibility.Hidden;
                Tan.Visibility = Visibility.Hidden;
                Exp.Visibility = Visibility.Hidden;
                Mod.Visibility = Visibility.Hidden;
                Log.Visibility = Visibility.Hidden;
                jedenPrzezX.Visibility = Visibility.Visible;
                eDoX.Visibility = Visibility.Visible;
                xDo3.Visibility = Visibility.Visible;
                PxSy.Visibility = Visibility.Visible;
                SinDoMinusJeden.Visibility = Visibility.Visible;
                CosDoMinusJeden.Visibility = Visibility.Visible;
                TanDoMinusJeden.Visibility = Visibility.Visible;
                ln.Visibility = Visibility.Visible;
                dms.Visibility = Visibility.Visible;
                deg.Visibility = Visibility.Visible;
            }
            else
            {
                sqrt.Visibility = Visibility.Visible;
                potega2.Visibility = Visibility.Visible;
                potegaY.Visibility = Visibility.Visible;
                dziesiecDoPotegi.Visibility = Visibility.Visible;
                Sin.Visibility = Visibility.Visible;
                Cos.Visibility = Visibility.Visible;
                Tan.Visibility = Visibility.Visible;
                Exp.Visibility = Visibility.Visible;
                Mod.Visibility = Visibility.Visible;
                Log.Visibility = Visibility.Visible;
                jedenPrzezX.Visibility = Visibility.Hidden;
                eDoX.Visibility = Visibility.Hidden;
                xDo3.Visibility = Visibility.Hidden;
                PxSy.Visibility = Visibility.Hidden;
                SinDoMinusJeden.Visibility = Visibility.Hidden;
                CosDoMinusJeden.Visibility = Visibility.Hidden;
                TanDoMinusJeden.Visibility = Visibility.Hidden;
                ln.Visibility = Visibility.Hidden;
                dms.Visibility = Visibility.Hidden;
                deg.Visibility = Visibility.Hidden;
            }
            
        }
    }
}
